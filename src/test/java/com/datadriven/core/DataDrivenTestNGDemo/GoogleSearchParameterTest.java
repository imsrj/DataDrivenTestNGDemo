package com.datadriven.core.DataDrivenTestNGDemo;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;



public class GoogleSearchParameterTest {
	//String driverPath = "C:\\Users\\sjaltare\\Documents\\chromedriver.exe";
	WebDriver driver;
    @Test
    @Parameters({"author","searchKey", "browser", "version", "os"})
    public void testGoogleSearchParameterFromXML( @Optional("Abc") String author,String searchKey, String browser, String version, String os) throws InterruptedException{

       // System.setProperty("webdriver.chrome.driver", driverPath);
        driver = Driver.createDriver(browser, version, os);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("https://google.com");

        WebElement searchText = driver.findElement(By.name("q"));
        //Searching text in google text box
        searchText.sendKeys(searchKey);

        System.out.println("Welcome ->"+author+" Your search key is->"+searchKey);
        System.out.println("Thread will sleep now");
       // Thread.sleep(3000);
        
        
        System.out.println("Value in Google Search Box = "+searchText.getAttribute("value") +" ::: Value given by input = "+searchKey);
        driver.quit();
        //verifying the value in google search box
       // AssertJUnit.assertTrue(searchText.getAttribute("value").equalsIgnoreCase(searchKey));

   }
    
    
}
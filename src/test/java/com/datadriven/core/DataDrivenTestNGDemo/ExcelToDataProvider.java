package com.datadriven.core.DataDrivenTestNGDemo;

import java.io.FileInputStream;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class ExcelToDataProvider {
	
	
	String xlFilePath = "/KRISHNA VOLUME/Jar Files/poi-3.16-beta1/TestData.xlsx";
    String sheetName = "Sheet1";
    
     
    @Test(dataProvider = "userData")
    public void fillUserForm(Object userName, Object passWord, Object dateCreated, Object noOfAttempts, Object result)
    {
       System.out.println("UserName: "+ userName);
       System.out.println("PassWord: "+ passWord);
       System.out.println("DateCreated: "+ dateCreated);
       System.out.println("NoOfAttempts: "+ noOfAttempts);
       System.out.println("Result: "+ result);
       System.out.println("*********************");
    }
     
     
    @DataProvider(name="userData")
    public Object[][] userFormData() throws Exception
    {
        Object[][] data = testData(xlFilePath, sheetName);
        return data;
    }
     
	
     public Object[][] testData(String excelFilePath, String sheetName) throws Exception{
    	 Object[][] excelData = null;
    	 FileInputStream fs = new FileInputStream("C:\\Users\\sjaltare\\Documents\\testdata.xlsx");
  	     XSSFWorkbook workbook = new XSSFWorkbook(fs);
  	     XSSFSheet sheet =  workbook.getSheet(sheetName);
  	     int totalRows = sheet.getLastRowNum() + 1;
	     XSSFRow row = sheet.getRow(0);
	     int totalColumns = row.getLastCellNum();
	     excelData = new Object[totalRows-1][totalColumns];
	     
	     for(int i=1; i<totalRows; i++) {
	    	 
	    	 for(int j=0; j<totalColumns; j++) {
	    		 excelData[i-1][j] = sheet.getRow(i).getCell(j);
	            //  System.out.println(excelData[i-1][j]);
	    	 }
	     }
	     
    	 return excelData;
	   //System.out.println("total rows:" + rowNum);
	   
     }
}

package com.datadriven.core.DataDrivenTestNGDemo;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
 
public final class Driver {
 
	private static final String USERNAME = "sarangjaltare22";
    private static final String ACCESS_KEY = "06503632-3d1a-47da-992f-c9bdca9778db";

    public static WebDriver createDriver(String browser, String version, String os) {
        // Should probably validate the arguments here
        try {
            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.setCapability(CapabilityType.BROWSER_NAME, browser);
            capabilities.setCapability(CapabilityType.VERSION, version);
            capabilities.setCapability(CapabilityType.PLATFORM, os);
            return new RemoteWebDriver(new URL("http://" + USERNAME + ":" + ACCESS_KEY + "@ondemand.saucelabs.com:80/wd/hub"),
                                       capabilities);
        } catch (MalformedURLException e) {
            throw new RuntimeException("Failure forming the URL to create a web driver", e);
        }
    }

    private Driver() {
        throw new AssertionError("This is a static class and shouldn't be instantiated.");
    }
}
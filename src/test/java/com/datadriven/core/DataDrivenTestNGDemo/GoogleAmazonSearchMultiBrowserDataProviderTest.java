package com.datadriven.core.DataDrivenTestNGDemo;

import org.testng.annotations.Test;
import org.testng.annotations.DataProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class GoogleAmazonSearchMultiBrowserDataProviderTest {
	
    WebDriver driver;
    @DataProvider(name = "DriverInfoProvider", parallel = true) // data providers force single threaded by default
    public Object[][] driverInfoProvider() {
        return new Object[][] {
            { "firefox", "50.0", "Windows 10" },
            { "chrome" , "60.0", "Windows 10" },
            // etc, etc
        };
    }
    @Test(dataProvider = "DriverInfoProvider")
    public void testGoogleSearchBar(String browser, String version, String os) {
        WebDriver driver = Driver.createDriver(browser, version, os);
        System.out.println(browser);
        try {
            // simple output to see the thread for each test method instance
            System.out.println("starting test in thread: " + Thread.currentThread().getName());
            // Putting this in a try catch block because you want to be sure to close the driver to free
            // up the resources even if the test fails
            driver.get("https://www.google.com");
            driver.findElement(By.name("q")).sendKeys("Hello, world");
        } finally {
            driver.quit();
        }
    }
    
    @Test(dataProvider = "DriverInfoProvider")
    public void testGetAmazonHomePage(String browser, String version, String os) {
        WebDriver driver = Driver.createDriver(browser, version, os);
        System.out.println(browser);
        try {
            // simple output to see the thread for each test method instance
            System.out.println("starting test in thread: " + Thread.currentThread().getName());
            // Putting this in a try catch block because you want to be sure to close the driver to free
            // up the resources even if the test fails
            driver.get("https://www.amazon.com");
          
        } finally {
            driver.quit();
        }
    }
}
